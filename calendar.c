#include<stdio.h>
#include<conio.h>
#include<stdbool.h>
#include<windows.h>

bool leap_checker(int year)
{
    if((year%400)==0)
        return 1;
    else if((year%100)==0)
        return 0;
    else if((year%4)==0)
        return 1;
    else
        return 0;
}
int day_count(int month,int  year)
{
    int days,i,count=0,md,yd;
    for(i=1;i<=(year-1);i++)
    {
       if(leap_checker(i)==1)
        count++;
    }
    yd=(year-1)*365+count;
    if((leap_checker(year)==1)&&(month>2))
    {
        if(month==4)
             md=(month-1)*30+1;
        if(month==5)
             md=(month-1)*30+1;
        if(month==6)
             md=(month-1)*30+2;
        if(month==7)
             md=(month-1)*30+2;
        if(month==8)
             md=(month-1)*30+3;
        if(month==9)
             md=(month-1)*30+4;
        if(month==10)
             md=(month-1)*30+4;
        if(month==11)
             md=(month-1)*30+5;
        if(month==12)
             md=(month-1)*30+5;
        if(month==3)
             md=(month-1)*30;
    }
    else
    {
        if(month==2)
             md=(month-1)*30+1;
        if(month==3)
             md=(month-1)*30-1;
        if(month==4)
             md=(month-1)*30;
        if(month==5)
             md=(month-1)*30;
        if(month==6)
             md=(month-1)*30+1;
        if(month==7)
             md=(month-1)*30+1;
        if(month==8)
             md=(month-1)*30+2;
        if(month==9)
             md=(month-1)*30+3;
        if(month==10)
             md=(month-1)*30+3;
        if(month==11)
             md=(month-1)*30+4;
        if(month==12)
             md=(month-1)*30+4;
        if(month==1)
             md=(month-1)*0;
    }
    days=yd+md;
    return days;
}

int day(int days)
{
    int mod=days%7+1;
    return mod+2;
}

void printer(int month, int year, int mod)
{
    int i,j,a=1;
    printf("\n\n\n\n\t\t\t\t\t------------------------------------\n");
    switch (month)
    {
    case 1:
        printf("\t\t\t\t\t|\t\t      JANUARY %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 2:
        printf("\t\t\t\t\t|\t\t     FEBRUARY %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 3:
        printf("\t\t\t\t\t|\t\t\tMARCH %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 4:
        printf("\t\t\t\t\t|\t\t\tAPRIL %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 5:
        printf("\t\t\t\t\t|\t\t\t  MAY %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 6:
        printf("\t\t\t\t\t|\t\t\t JUNE %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 7:
        printf("\t\t\t\t\t|\t\t\t JULY %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 8:
        printf("\t\t\t\t\t|\t\t       AUGUST %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 9:
        printf("\t\t\t\t\t|\t\t    SEPTEMBER %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 10:
        printf("\t\t\t\t\t|\t\t      OCTOBER %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 11:
        printf("\t\t\t\t\t|\t\t     NOVEMBER %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    case 12:
        printf("\t\t\t\t\t|\t\t     DECEMBER %4d |\n\t\t\t\t\t------------------------------------\n",year);
        break;
    default:
        printf("\n\n\n\n\n\n\n\t\t\t\t\t\t\tERROR\n\n");
    }
    if((month==1)||(month==3)||(month==5)||(month==7)||(month==8)||(month==10)||(month==12))
    {
        printf("\t\t\t\t\t| SAT| SUN| MON| TUE| WED| THU| FRI|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
        _sleep(100);
        for(i=1;i<=6;i++)
        {
            for(j=1;j<=7;j++,a++)
            {
                if((a<(mod))||(a>(31+mod-1)))
                {
                    printf("|    ");
                    _sleep(40);
                }
                else
                {
                    printf("|  %2d",a-mod+1);
                    _sleep(40);
                }
            }
            printf("|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
            _sleep(40);
        }
    }
    else if((month==4)||(month==6)||(month==9)||(month==11))
    {
        printf("\t\t\t\t\t| SAT| SUN| MON| TUE| WED| THU| FRI|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
        _sleep(100);
        for(i=1;i<=6;i++)
        {
            for(j=1;j<=7;j++,a++)
            {
                if((a<(mod))||(a>(30+mod-1)))
                {
                    printf("|    ");
                    _sleep(40);
                }
                else
                {
                    printf("|  %2d",a-mod+1);
                    _sleep(40);
                }
            }
            printf("|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
            _sleep(40);
        }
    }
    else if((month==2)&&((year%4)==0))
    {
        printf("\t\t\t\t\t| SAT| SUN| MON| TUE| WED| THU| FRI|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
        _sleep(100);
        for(i=1;i<=6;i++)
        {
            for(j=1;j<=7;j++,a++)
            {
                if((a<(mod))||(a>(29+mod-1)))
                {
                    printf("|    ");
                    _sleep(40);
                }
                else
                {
                    printf("|  %2d",a-mod+1);
                    _sleep(40);
                }
            }
            printf("|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
            _sleep(40);
        }
    }
    else if(month==2)
    {
        printf("\t\t\t\t\t| SAT| SUN| MON| TUE| WED| THU| FRI|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
        _sleep(100);
        for(i=1;i<=6;i++)
        {
            for(j=1;j<=7;j++,a++)
            {
                if((a<(mod))||(a>(28+mod-1)))
                {
                    printf("|    ");
                    _sleep(40);
                }
                else
                {
                    printf("|  %2d",a-mod+1);
                    _sleep(40);
                }
            }
            printf("|\n\t\t\t\t\t------------------------------------\n\t\t\t\t\t");
            _sleep(40);
        }
    }
    else
        printf("\n\t\t\t\t\t\t MONTH OUT OF RANGE\n");
}

main()
{
    printf("\n\n\n\n\n\n\n\n\n\t\t\t\t\t    CALENDAR GENERATOR beta 1.0\n\n");
    int month,year,days,mod,i;
    printf("\t\t\t\t\t\t    YEAR  : ");
    scanf("%d",&year);
    printf("\t\t\t\t\t\t    MONTH : ");
    scanf("%d",&month);
    days=day_count(month,year);
    mod=day(days);
    system("cls");
    _sleep(100);
    printer(month,year,mod);
    printf("\n\n\n\t\t\t\t     PRESS ENTER TO SEE THE WHOLE YEAR CALENDAR\n");
    getch();
    char a=getchar();
    if(a=='\n')
    {
        system("cls");
        printf("\n\n\t\t\t\t\t\t      YEAR  %4d",year);
        for(i=1;i<=12;i++)
        {
            days=day_count(i,year);
            mod=day(days);
            printf("\n");
            printer(i,year,mod);
            printf("\n");
        }
    }
}